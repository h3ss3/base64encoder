package h3ss3.base64encoder.model;

import java.io.File;
import java.util.regex.Pattern;

/**
 * 
 * @author "Hessé <sylvain.carite@gmail.com>"
 *
 */
public class ImageMatcher {
	
	private Pattern extension;
	
	public ImageMatcher() {
		extension = Pattern.compile("^.+\\.(jpe?g|gif|png)$");
	}

	public boolean match(File file) {
		return file.isFile() && extension.matcher(file.getName().toLowerCase()).find();
	}
}
