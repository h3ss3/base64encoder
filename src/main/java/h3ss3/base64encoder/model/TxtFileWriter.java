package h3ss3.base64encoder.model;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;

/**
 * 
 * @author "Hessé <sylvain.carite@gmail.com>"
 *
 */
public class TxtFileWriter {
	
	private static final String EOF = "\n";

	private Writer writer;
	
	public TxtFileWriter(String filepath) throws UnsupportedEncodingException, FileNotFoundException {
		writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filepath), "UTF-8"));
	}
	
	public void writeRecord(File image, String content) throws IOException {
		writer.write(image.getName() + EOF);
		writer.write(content + EOF);
	}
	
	public void close() throws IOException {
		writer.close();
	}
}
