package h3ss3.base64encoder.model;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Base64;

/**
 * 
 * @author "Hessé <sylvain.carite@gmail.com>"
 *
 */
public class Base64Encoder {

	public String encode(File image) throws EncoderException {
		try {
			byte[] reader = Files.readAllBytes(image.toPath());
			byte[] encoded = Base64.getEncoder().encode(reader);
			
			return new String(encoded);
		} catch (IOException e) {
			throw new EncoderException(image, e);
		}
	}
}
