package h3ss3.base64encoder.model;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 
 * @author "Hessé <sylvain.carite@gmail.com>"
 *
 */
public class FilepathGenerator {
	
	public String generateFilepath(File directory) {
	    String dt = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss").format(new Date());
	    String filepath = directory.getAbsolutePath() + "/base64_generated_"+dt+".txt";
	    
	    return filepath;
	}
}
