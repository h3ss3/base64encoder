package h3ss3.base64encoder.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

/**
 * 
 * @author "Hessé <sylvain.carite@gmail.com>"
 *
 */
public class ConsoleModel extends Observable {

	private List<String> lines = new ArrayList<>();
	
	public void addLine(String line) {
		lines.add(line);
		setChanged();
		notifyObservers();
	}
	
	public String[] getLines() {
		return lines.toArray(new String[lines.size()]);
	}
}
