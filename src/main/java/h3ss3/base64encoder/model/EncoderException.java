package h3ss3.base64encoder.model;

import java.io.File;

/**
 * 
 * @author "Hessé <sylvain.carite@gmail.com>"
 *
 */
public class EncoderException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8858110341014278189L;

	public EncoderException(File image, Throwable cause) {
		super(cause);
	}
}
