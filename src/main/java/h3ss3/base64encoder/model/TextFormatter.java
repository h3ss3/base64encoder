package h3ss3.base64encoder.model;

/**
 * 
 * @author "Hessé <sylvain.carite@gmail.com>"
 *
 */
public class TextFormatter {
	
	private String template;
	
	public TextFormatter(String template) {
		this.template = template;
	}

	public String formatImageContent(String content) throws TextFormatterTemplateException {
		if (!template.contains("{base64}")) {
			throw new TextFormatterTemplateException(template, "{base64}");
		}
		return template.replaceAll("\\{base64\\}", content);
	}
}
