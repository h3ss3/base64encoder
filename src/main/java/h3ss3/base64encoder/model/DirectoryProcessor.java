package h3ss3.base64encoder.model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author "Hessé <sylvain.carite@gmail.com>"
 *
 */
public class DirectoryProcessor {
	
	private ConsoleModel model;
	private List<File> acceptedImages;
	private ImageMatcher imageMatcher;
	private Base64Encoder encoder;
	
	public DirectoryProcessor(ConsoleModel model) {
		this.model = model;
		imageMatcher = new ImageMatcher();
		encoder = new Base64Encoder();
	}
	
	public void handleDirectorySelection(File directory) {
		filterImages(directory);
	}
	
	public void handleGeneration(String template, String filepath) {
		encodeImages(template, filepath);
	}
	
	protected void filterImages(File directory) {
		acceptedImages = new ArrayList<>();

		File[] files = directory.listFiles();
		model.addLine(files.length+" fichiers trouvés dans le répertoire.");
		for(File f : files) {
			if (imageMatcher.match(f)) {
				acceptedImages.add(f);
			}
		}
		model.addLine(acceptedImages.size()+" images trouvées dans le répertoire.");
		model.addLine("");
	}

	protected void encodeImages(String template, String filepath) {
	    TxtFileWriter writer = null;
	    
		try {
			writer = new TxtFileWriter(filepath);
			TextFormatter formatter = new TextFormatter(template);
			model.addLine("Fichier TXT créé : "+filepath);
			model.addLine("Encodage en cours...");
		    
		    int success = 0;
		    int failure = 0;
			for(File image : acceptedImages) {
				try {
					writer.writeRecord(image, formatter.formatImageContent(encoder.encode(image)));
					success++;
				} catch (EncoderException e) {
					failure++;
					model.addLine("ERREUR lors de l'encodage de "+image.getName()+" ; cause : "+e.getMessage());
				} catch (IOException e) {
					failure++;
					model.addLine("ERREUR lors de l'écriture dans le txt de "+image.getName()+" ; cause : "+e.getMessage());
				}
			}
			model.addLine("Encodage terminé");
			model.addLine(success+" succès et "+failure+" échecs");
		} catch (UnsupportedEncodingException e1) {
			model.addLine("ERREUR lors de la création du fichier "+filepath+" : encodage incorrect ; cause : "+e1.getMessage());
		} catch (FileNotFoundException e1) {
			model.addLine("ERREUR lors de la création du fichier "+filepath+" ; cause : "+e1.getMessage());
	    } catch (TextFormatterTemplateException e) {
			model.addLine("ERREUR lors de la génération du fichier "+filepath+" : le modèle "+e.getTemplate()+" ne contient pas la balise "+e.getTag());
		} finally {
			if (writer != null) {
				try {
					writer.close();
				} catch (IOException e) {
					model.addLine("ERREUR lors de la fermeture du fichier "+filepath+" ; cause : "+e.getMessage());
				}
			}
			model.addLine("");
		}
	}
	
}
