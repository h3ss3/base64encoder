package h3ss3.base64encoder.model;

/**
 * 
 * @author "Hessé <sylvain.carite@gmail.com>"
 *
 */
public class TextFormatterTemplateException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7199500961675280030L;
	private String template;
	private String tag;

	public TextFormatterTemplateException(String template, String tag) {
		this.template = template;
		this.tag = tag;
	}
	
	public String getTemplate() {
		return template;
	}
	
	public String getTag() {
		return tag;
	}
}
