package h3ss3.base64encoder;

import java.util.Observable;
import java.util.Observer;

import h3ss3.base64encoder.model.ConsoleModel;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.TextArea;

/**
 * 
 * @author "Hessé <sylvain.carite@gmail.com>"
 *
 */
public class Console implements Observer {

	private TextArea console;
	private ConsoleModel model;
	
	public Console(TextArea console, ConsoleModel model) {
		this.console = console;
		this.model = model;
		model.addObserver(this);
	}

	@Override
	public void update(Observable o, Object arg) {
		console.setText("");
		console.appendText(String.join("\n", model.getLines()));
	}
}
