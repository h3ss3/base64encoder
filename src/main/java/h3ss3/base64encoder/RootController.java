package h3ss3.base64encoder;

import java.io.File;

import h3ss3.base64encoder.model.ConsoleModel;
import h3ss3.base64encoder.model.DirectoryProcessor;
import h3ss3.base64encoder.model.FilepathGenerator;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.DirectoryChooser;

/**
 * 
 * @author "Hessé <sylvain.carite@gmail.com>"
 *
 */
public class RootController {

	@FXML private Button directoryBtn;
	@FXML private Label directoryLabel;
	@FXML private Button encodeBtn;
	@FXML private Label encodeLabel;
	@FXML private TextField template;
	@FXML private TextArea console;

	private FilepathGenerator filepathGenerator;
	private ConsoleModel model;
	private DirectoryProcessor directoryProcessor;
	
	private File directory;
	
    @FXML
    private void initialize() {
    	model = new ConsoleModel();
    	new Console(console, model);
    	filepathGenerator = new FilepathGenerator();
    }
	
	@FXML
	public void selectDirectory() {
		DirectoryChooser chooser = new DirectoryChooser();
		chooser.setTitle("Répertoire d'images");
		directory = chooser.showDialog(null);
		if (directory != null) {
			directoryProcessor = new DirectoryProcessor(model);
			directoryLabel.setText(directory.getPath());
			directoryProcessor.handleDirectorySelection(directory);
		}
	}
	
	@FXML
	public void encode() {
		if (directoryProcessor == null) {
			model.addLine("Sélectionner d'abord un répertoire d'images...");
		} else {
			String filepath = filepathGenerator.generateFilepath(directory);
			encodeLabel.setText(filepath);
			directoryProcessor.handleGeneration(template.getText(), filepath);
		}
	}
}
